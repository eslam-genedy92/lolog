'use strict';

module.exports = function(app) {
  // Install a `/` route that returns server status
  var bodyparser = require('body-parser');
  // var ulits = require('ulit');
  var loopback = require('loopback');
  var User = app.models.reviewer;
  var review = app.models.review;
  var coffeeshop = app.models.coffeeshop;
  var router = app.loopback.Router();
  app.use(loopback.token({
    model: app.models.accessToken,
    currentUserLiteral: 'me'
  }));
  router.get('/status', app.loopback.status());
  app.use(router);
  app.use(bodyparser.urlencoded({ extended: true }));
  app.use(bodyparser.json());
  
   //=========================home===========================//
   router.get('/',function(req,res){
  //  var reviewss = review.find({
  //    fields:{
  //      id:true,
  //      date:true,
  //      rating:true,
  //      mail:true,
  //      comments:true,
  //      coffeeshop:true
  //    }
  //   },function(err){
  //     console.log('date is created');
  //   });
    res.render('../client/views/home.ejs');
   // console.log(reviewss);
  });
  //=========signup======================================//
  router.get('/signup',(req,res)=>{
    res.render('../client/views/signup.ejs');
  });

  router.post('/signup',(req,res)=>{
    var email = req.body.email;
    var password = req.body.password;
    var repeat = req.body.repeat;
    User.create({ email: email, password: password }, function(err, userInstance) {
      console.log(userInstance);
    });
    res.redirect('/login');
  });

  //============================login=================//
  router.get('/login',(req,res)=>{
    res.render('../client/views/login.ejs');
  });


  router.post('/login', function(req, res) {
  // var coffee =  coffeeshop.find({
  //     name:'true',
  //     city:'true',
  //     once: false
  //   },function(err){
  //     console.log('==================================================================');
  //   }); 
    User.login({
      email: req.body.email,
      password: req.body.password
    }, 'reviewer', function(err, token) {
      if (err) {
        res.render('response', { //render view named 'response.ejs'
          title: 'Login failed',
          content: err,
          redirectTo: '/signup',
          redirectToLinkText: 'Try again'
        });
        return;
      }
      res.render('../client/views/ratings.ejs', { //login user and render 'home' view
        email: req.body.email,
        accessToken: token.id,
      
      });
    });
  });
 

  //============================logout=========================//
  router.get('/logout', function(req, res, next) {
    if (!req.accessToken) return res.sendStatus(401);
    User.logout(req.accessToken.id, function(err) {
      if (err){ 
        return next(err);
        console.log('err');
      }
      res.redirect('/');
    });
  });
  //=========================rating===========================//
  router.get('/add-review',(req,res)=>{
   res.render('../client/views/home.ejs');
  });
// ##################################################################
  router.post('/add-review',(req,res)=>{
   var comments = req.body.comment ;
   var rating = req.body.rating ; 
   var coffeeshop = req.body.cafe;
   var mail = req.body.mail;
   var time = req.body.time;
  var reviewa= review.create({
    date: Date.now() ,
        rating: rating,
        comments: comments,
        mail: mail,
        coffeeshop: coffeeshop,
   },function(err,reviewa){
      console.log(reviewa);
   });
 //  console.log(reviewa);
   
  //  var myreview = review.find({
  //   where:{mail:mail}
  //  },function(err,myreview){
  //   console.log(myreview);
  //  });
 
});

// router.get('/allreviews',(req,res)=>{
 
// });

router.get('/allreviews',(req,res)=>{
  var revis = review.find({
    fields: {rating: true, comments: true, mail: true,coffeeshop:true,date:true}
  },function(err,revis){
    console.log(revis);
  });
  console.log(revis);
 res.send('revis');
 
});

router.get('/myreview',(req,res)=>{
 
});
}