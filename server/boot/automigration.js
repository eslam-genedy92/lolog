'use strict';
var async = require('async');
module.exports = function(app) {
  //data sources
  var mongoDs = app.dataSources.loo;
  //create all models
  async.parallel({
    reviewers: async.apply(createReviewers),
   coffeeshops: async.apply(createCoffeeShops),
   reviews:async.apply(createreviews),
  }, function(err, results) {
    if (err) throw err;
    // createReviews(results.reviewers , results.coffeeshops , function(err) {
    //   console.log('> models created sucessfully');
    // });
  });
  function createReviewers(cb) {
    mongoDs.automigrate('reviewer', function(err) {
      if (err) return cb(err);
      var Reviewer = app.models.Reviewer;
      Reviewer.create([{
        email: 'foo@bar.com',
        password: 'foobar',
      }, {
        email: 'john@doe.com',
        password: 'johndoe',
      }, {
        email: 'jane@doe.com',
        password: 'janedoe',
      }], cb);
    });
  }
  //create coffee shops
  function createCoffeeShops(cb) {
    mongoDs.automigrate('coffeeshop', function(err) {
      if (err) return cb(err);
      var coffeeShop = app.models.coffeeshop;
      coffeeShop.create([{
        name: 'Bel 12Cafe',
        city: 'Vancouver',
        once:true,
      }, {
        name: 'Three Bees Coffee House',
        city: 'San Mateo',
        once:true,

      }, {
        name: 'Caffe Artigiano',
        city: 'Vancouver',
        once:true,

      }], cb);
    });
  }


  function createreviews(cb) {
    mongoDs.automigrate('review', function(err) {
      if (err) return cb(err);
      var review = app.models.review;
      var DAY_IN_MILLISECONDS = 1000 * 60 * 60 * 24;
      review.create([{
        date: Date.now() - (DAY_IN_MILLISECONDS * 4),
        rating: 5,
        comments: 'A very good coffee shop.',
        mail: 'jan12e@doe.com',
        coffeeshop: 'Caffe Artigiano',
       // publisherId: reviewers[0].id,
        // coffeeShopId: coffeeShops[0].id,
      }, {
        date: Date.now() - (DAY_IN_MILLISECONDS * 3),
        rating: 5,
        comments: 'Quite pleasant.',
        mail: 'jane@doe.com',
        coffeeshop: 'Caffe Artigiano',
        //publisherId: reviewers[1].id,
        // coffeeShopId: coffeeShops[0].id,
      }, {
        date: Date.now() - (DAY_IN_MILLISECONDS * 2),
        rating: 4,
        comments: 'It was ok.',
        mail: 'jane@doe.com',
        coffeeshop: 'Caffe Artigiano',
        //publisherId: reviewers[1].id,
        // coffeeShopId: coffeeShops[1].id,
      }, {
        date: Date.now() - (DAY_IN_MILLISECONDS),
        rating: 4,
        comments: 'I go here everyday.',
        mail: 'jane@doe.com',
        coffeeshop: 'Caffe Artigiano',
       // publisherId: reviewers[2].id,
        // coffeeShopId: coffeeShops[2].id,
      }], cb);
    });
  }


  //create reviews
//   function createReviews(reviewers, cb) {
//     mongoDs.automigrate('Review', function(err) {
//       if (err) return cb(err);
//       var Review = app.models.Review;
//       var DAY_IN_MILLISECONDS = 1000 * 60 * 60 * 24;
//       Review.create([{
//         date: Date.now() - (DAY_IN_MILLISECONDS * 4),
//         rating: 5,
//         comments: 'A very good coffee shop.',
//        // publisherId: reviewers[0].id,
//         // coffeeShopId: coffeeShops[0].id,
//       }, {
//         date: Date.now() - (DAY_IN_MILLISECONDS * 3),
//         rating: 5,
//         comments: 'Quite pleasant.',
//         //publisherId: reviewers[1].id,
//         // coffeeShopId: coffeeShops[0].id,
//       }, {
//         date: Date.now() - (DAY_IN_MILLISECONDS * 2),
//         rating: 4,
//         comments: 'It was ok.',
//         //publisherId: reviewers[1].id,
//         // coffeeShopId: coffeeShops[1].id,
//       }, {
//         date: Date.now() - (DAY_IN_MILLISECONDS),
//         rating: 4,
//         comments: 'I go here everyday.',
//         publisherId: reviewers[2].id,
//         // coffeeShopId: coffeeShops[2].id,
//       }], cb);
//     });
//   }
};